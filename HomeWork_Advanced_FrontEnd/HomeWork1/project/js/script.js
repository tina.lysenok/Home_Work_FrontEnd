$(function ourServices() {

    $("#active-tabs").on("click", ".menu-top-link", function () {

        let item = $("#active-tabs .menu-top-link");
        // Удаляем классы active
        item.removeClass("active");
        // Добавляем классы active
        $(this).addClass("active");
        return false;
    });
});

$(document).ready(() => {
    $('#toggle1').on('click', () => {
       $('#cartMenu').slideToggle('fast');
    });
    $('#toggle2').on('click', () => {
       $('#accountMenu').slideToggle('fast');
    });
    $('#toggle3').on('click', () => {
       $('#accountMenu2').slideToggle('fast');
    });
    $('#toggle4').on('click', () => {
       $('#helpMenu').slideToggle('fast');
    });

    $('#cartMenu').on('mouseleave', () => {
        $('#cartMenu').toggle();
    });
    $('#accountMenu').on('mouseleave', () => {
        $('#accountMenu').toggle();
    });
    $('#accountMenu2').on('mouseleave', () => {
        $('#accountMenu2').toggle();
    });
    $('#helpMenu').on('mouseleave', () => {
        $('#helpMenu').toggle();
    });

});