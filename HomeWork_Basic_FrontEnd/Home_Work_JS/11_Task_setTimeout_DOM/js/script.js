/*------------1 функция-------------*/
//проверка введенного значения пользователя на число
function checkNumValue(num) {
    while (isNaN(num)
        || Number(num) <= 0
        || !Number.isInteger(Number(num))
        ) {
        if (isNaN(num)) {
            num = prompt('Please enter a number value.');
        } else if (Number(num) <= 0) {
            num = prompt('Age value should be greater than 0.');
        } else if (!Number.isInteger(Number(num))) {
            num = prompt("Age value couldn't be a float number.");
        }
    }
    return Number(num);
}

let numList = checkNumValue(prompt('Please insert number of items', ''));

/*------------2 функция-------------*/
//используем введенные пользователем значения для их вывода на экран
let userAnswer;
let newListElement;
let arrUserAnswers = [];

function showUserAnswersInList() {
    for (let i = 0; i < numList; i++) {
        userAnswer = prompt('Insert info', '');

        arrUserAnswers.push(userAnswer);

        newListElement = document.createElement('li');
        newListElement.innerHTML = userAnswer;
        listUserAnswers.appendChild(newListElement);
    }
}

showUserAnswersInList();

/*------------3 функция-------------*/
//использование ф-ии map, копируем массив данных в отдельный массив и выводим его на страницу
let newArray = arrUserAnswers.map(function (value) {
    return value;
});
console.log(newArray);

/*------------4 функция-------------*/
//удаление всех элементов списка
setTimeout(function () {
    while (listUserAnswers.lastChild) {
        listUserAnswers.removeChild(listUserAnswers.lastChild);
    }
}, 1000 * 10);
/*------------5 функция-------------*/

//работа таймера
function startTimer() {
    let myTimer = document.getElementById('myTimer');
    let time = myTimer.innerHTML; //00:00:11
    let arrTime = time.split(':'); //[00, 00, 11]
    let hours = arrTime[0]; //00
    let minutes = arrTime[1]; //00
    let seconds = arrTime[2]; //11
    if (seconds == 0) {
        alert("It's a deadly virus((( All your items was deleted!");
        window.location.reload();
        return;
    }
    seconds--;
    if (seconds < 10) {
        seconds = '0' + seconds;
    }
    document.getElementById('myTimer').innerHTML = hours + ":" + minutes + ":" + seconds;
    setTimeout(startTimer, 1000);
}

startTimer();





















