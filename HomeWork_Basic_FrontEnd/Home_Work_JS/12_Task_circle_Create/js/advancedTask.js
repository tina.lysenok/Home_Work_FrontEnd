function showInputFields() {
    let inputDiameter = document.createElement("input");
    inputDiameter.setAttribute("type", "text");
    inputDiameter.setAttribute("id", "diameter");
    inputDiameter.setAttribute("placeholder", "Enter diameter value");
    element.appendChild(inputDiameter);

    let btn = document.createElement("button");
    btn.setAttribute("id", "drawCircle");
    btn.setAttribute("onclick", "createCircle()");
    btn.innerHTML = "Draw Circle";
    element.appendChild(btn);

    document.getElementById("inputFieldsForCircle").style.display = "block";
    document.getElementById("createCircle").style.display = "none";
}

let colorArr = ["red", "black", "yellow", "violet", "red", "green", "blue", "lightgreen"];

function drawCircle() {
    let diameterCircle = document.getElementById("diameter").value;
    let obj = document.getElementById("containerCircle");

    let tmp,
        radius = Number(diameterCircle) / 2;

    for (let c = 0; c < 100; c++) {
        for (let i = 0; i <= 2 * radius; i++) {
            let height = Math.abs(radius - i);
            let chord = 2 * Math.sqrt(radius * radius - height * height);
            tmp = document.createElement("div");
            tmp.style.height = "1px";
            tmp.style.width = chord + "px";
            for (let j = 0; j < colorArr.length; j++) {
                tmp.style.backgroundColor = colorArr[Math.round(Math.random() * j)];
            }
           /* let c = Math.round(Math.random() * 100);
            tmp.style.backgroundColor = "rgb(" + c +',' + c + ',' + c + ")";*/
            obj.appendChild(tmp);
        }
    }
}

function createCircle() {
    setTimeout(drawCircle, 1000);
}

function divTest() {
    let c = document.getElementById("containerCircle");
    document.body.removeChild(c);
}

let element = document.getElementById("inputFieldsForCircle");
