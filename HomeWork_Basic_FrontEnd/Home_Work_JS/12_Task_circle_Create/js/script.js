function showInputFields() {
    let inputDiameter = document.createElement("input");
    inputDiameter.setAttribute("type", "text");
    inputDiameter.setAttribute("id", "diameter");
    inputDiameter.setAttribute("placeholder", "Enter diameter value");
    element.appendChild(inputDiameter);

    let inputColor = document.createElement("input");
    inputColor.setAttribute("type", "text");
    inputColor.setAttribute("id", "color");
    inputColor.setAttribute("placeholder", "Enter color");
    element.appendChild(inputColor);

    let btn = document.createElement("button");
    btn.setAttribute("id", "drawCircle");
    btn.setAttribute("onclick", "createCircle()");
    btn.innerHTML = "Draw Circle";
    element.appendChild(btn);

    document.getElementById("inputFieldsForCircle").style.display = "block";
    document.getElementById("createCircle").style.display = "none";
}


function drawCircle() {
    let diameterCircle = document.getElementById('diameter').value;
    let colorCircle = document.getElementById('color').value;
    let obj = document.getElementById('containerCircle');
    let tmp,
        radius = Number(diameterCircle)/2;
    for (let i = 0; i <= 2 * radius; i++) {
        let height = Math.abs(radius - i);
        let chord = 2 * Math.sqrt(radius * radius - height * height);
        tmp = document.createElement('div');
        tmp.style.height = '1px';
        tmp.style.width = chord + 'px';
        tmp.style.backgroundColor = colorCircle;
        obj.appendChild(tmp);
    }
}

function createCircle() {
    setTimeout(drawCircle, 1000);
}

let element = document.getElementById("inputFieldsForCircle");
