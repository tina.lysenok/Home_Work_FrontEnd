$(document).ready(function () {
    let result = 0;                      //default value for result
    let prevEntry = 0;                   //value a
    let operation = null;                //default value of operation if user picked nothing
    let currentEntry = "0";              //value b
    let mrc = [];                        //save values of numbers in array

    updateScreen(result);

    $(".button").on("click", function () {
        let btnPressed = $(this).val();    // pull the value from input with class "button"
        console.log(btnPressed);

        if (btnPressed === "C") {          //reset previous operation
            result = 0;
            currentEntry = "0";
        } else if (btnPressed === "mrc") {
            mrc.push(+currentEntry);       //add values to array/save value in memory
            result = 0;
            currentEntry = "0";
        } else if (btnPressed === "m+") {
            currentEntry = mrc.reduce(function (first, second) { //sum all items in array and put sum in input field
                return first + second;
            });
        } else if (btnPressed === "m-") {
            currentEntry = mrc.reduce(function (first, sec) {    //difference of numbers in array
                return first - sec;
            });
        } else if (btnPressed === "pi") {
            currentEntry = Math.PI;
        } else if (btnPressed === "sqrt") {
            currentEntry = Math.sqrt(currentEntry);
        } else if (btnPressed === "round") {
            currentEntry = Math.round(currentEntry);
        } else if (btnPressed === "floor") {
            currentEntry = Math.floor(currentEntry);
        } else if (btnPressed === "ceil") {
            currentEntry = Math.ceil(currentEntry);
        } else if (btnPressed === "+/-") {
            currentEntry *= -1;
        } else if (btnPressed === ".") {
            currentEntry += '.';
        } else if (btnPressed === "%") {
            currentEntry = currentEntry / 100;
        } else if (btnPressed === "1/x") {
            currentEntry = 1 / currentEntry;
            if (currentEntry == Infinity) {                //if currentEntry = 0, result in input also 0
                currentEntry = 0;
            }
        } else if (isNumber(btnPressed)) {                 //check btn with Number value
            if (currentEntry === "0") {
                currentEntry = btnPressed;                 //if 0 in first input, then currentEntry = Number which you pick by using btn
            } else if (currentEntry === 0) {
                currentEntry = btnPressed;                 //if 0 after some math operation, currentEntry = Number which you pick by using btn
            } else {
                currentEntry = currentEntry + btnPressed;  //example: you need number 70, so you pick 7 btn + 0 btn
            }
        } else if (isOperator(btnPressed)) {               //check btn with Operator value
            prevEntry = parseFloat(currentEntry);          //prevEntry = Number : you need to check procedure before math =>
                                                           //=> operation, so it need to be 10 + 10
            operation = btnPressed;                        //operation === true : +/-*
            currentEntry = "";                             //currentEntry could be a number
        } else if (btnPressed === "=") {
            currentEntry = operate(prevEntry, currentEntry, operation); //use the func operate for math operations
            operation = null;
        }
        updateScreen(currentEntry);                       //use the function updateScreen for update displaying value in input
    });
});

updateScreen = function (displayValue) {                  //add the value into input
    displayValue = displayValue.toString();
    $("#inputField").val(displayValue.substring(0, 10));  //value can't be more than ten-digit value
};

isNumber = function (value) {
    return !isNaN(value);                                 //check if value is equal to Number
};

isOperator = function (value) {                            //check if value is equal to Operator
    return value === "/" || value === "*"
        || value === "+" || value === "-";
};

operate = function (a, b, operation) {
    if (isNaN(a) && !isNaN(b) && isOperator(operation)) {
        return "0";
    }

    if (b === "") {
        return a;
    }

    a = parseFloat(a);
    b = parseFloat(b);

    console.log(a, b, operation);

    if (operation === "+") {
        return a + b;
    }
    if (operation === "-") {
        return a - b;
    }
    if (operation === "*") {
        return a * b;
    }
    if (operation === "/") {
        return a / b;
    }
};


















