/*-------CheckBox_Example------*/

/*// создаем <link rel="stylesheet" href="light|dark.css">
let head = document.head,
    link = document.createElement('link');
    link.rel = 'stylesheet';
// проверяем значение из localStorage если dark то темная тема
if (localStorage.getItem('themeStyle') === 'Gta') {
    link.href = 'css/Gta.css'; // ссылка на темный стиль
    document.getElementById('switch-1').setAttribute('checked', true); // переключаем чекбокс в положение "темная тема"
}
// по умолчанию светлая тема
else {
    link.href = 'css/Usual.css'; // ссылка на светлый стиль
}
head.appendChild(link); // вставляем <link rel="stylesheet" href="light|dark.css"> в шапку страницы между темаги head

// событие при переключении чекбокса
document.getElementById('switch-1').addEventListener('change', ev => {
    let btn = ev.target;
    // если чекбокс включен
    if (btn.checked) {
        link.href = 'css/Gta.css'; // сключаем темную тему
        localStorage.setItem('themeStyle', 'Gta'); // записываем значение в localStorage
    }
    else {
        link.href = 'css/Usual.css'; // включаем светлую тему
        localStorage.setItem('themeStyle', 'Usual'); // записываем значение в localStorage
    }
});*/

/*-------Button_Example------*/

// создаем <link rel="stylesheet" href="light|dark.css">
let head = document.head,
    link = document.createElement('link');
    link.rel = 'stylesheet';
// проверяем значение из localStorage если dark то темная тема
if (localStorage.getItem('themeStyle') === 'Gta') {
    link.href = 'css/Gta.css'; // ссылка на темный стиль
} else { // по умолчанию светлая тема
    link.href = 'css/Usual.css'; // ссылка на светлый стиль
}
head.appendChild(link); // вставляем <link rel="stylesheet" href="light|dark.css"> в шапку страницы между темаги head

// событие при нажатии кнопки
let btn = document.getElementById("changeTheme");
let init = 0;

btn.addEventListener("click", function () {
    ++init;
    console.log(init);
    //GTA Theme
    if (init == 1 || init % 2 >= 1) {
        link.href = 'css/Gta.css'; // сключаем темную тему
        localStorage.setItem('themeStyle', 'Gta'); // записываем значение в localStorage
    }
    //usually theme
    if (init % 2 == 0) {
        link.href = 'css/Usual.css'; // включаем светлую тему
        localStorage.setItem('themeStyle', 'Usual'); // записываем значение в localStorage
    }
});






















