$(window).scroll(function() {
    // --- Параллакс-скролинг для фона шапки Главной ---
    var howMuchScrolled = $(window).scrollTop();
    var verticalScrollSpeed = howMuchScrolled / 4; // скорость вертикального скроллинга фона
    var horizontalScrollSpeed = howMuchScrolled / 40; // скорость горизонтального скроллинга фона
    // скорость скроллинга медленнее, чем больше делитель

    var scrollDown = 0 + verticalScrollSpeed;
    var scrollRight = 50 + horizontalScrollSpeed;
    // 0 и 50 - изначальное положение фона

    $('#main-header-wrap').css('background-position', ((scrollRight)) + '%' + ((scrollDown)) + 'px');
});