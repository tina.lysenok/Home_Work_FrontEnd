function userAccess() {
    let name = null;
    let age = null;

    let checkAge = function () {
        while (isNaN(age)
            || Number(age) <= 0
            || !Number.isInteger(Number(age)
            || age == null)
            ) {
            if (isNaN(age)) {
                age = prompt('Please enter a number value.');
            } else if (Number(age) <= 0) {
                age = prompt('Age value should be greater than 0.');
            } else if (!Number.isInteger(Number(age))) {
                age = prompt("Age value couldn't be a float number.");
            } else {
                age = prompt("Please enter your age!");
            }
        }
    };

    let checkName = function () {
        while (name === null
        || name === ""
        || !isNaN(name)) {
            name = prompt('Please using alphabetical values for name statement.');
        }
    };

    this.getUser = function () {
        name = prompt('Please enter your name', '');
        checkName();
        age = Number(prompt('Please enter your age', ''));
        checkAge();
    };

    this.checkAccess = function () {
        if (Number(age) < 18) {
            alert('You are not allowed to visit this website.');
        } else if (Number(age) >= 18 && Number(age) <= 22) {
            let confirmValue = confirm('Are you sure you want to continue?');
            if (confirmValue == 1) {
                return alert('Welcome, ' + name);
            } else {
                alert('You are not allowed to visit this website.');
            }
        } else {
            alert('Welcome, ' + name);
        }
    }
}

let user = new userAccess();
user.getUser();
user.checkAccess();

