function checkNumber(num) {
    while (num == null
    || num === ""
    || isNaN(num)
    || !Number.isInteger(Number(num))
    || Number(num) === 1
    || Number(num) <= 0) {
        if (num == null || num === "" || isNaN(num)) {
            num = prompt("Enter a number value.", "");
        } else if (!Number.isInteger(Number(num))) {
            num = prompt("Please don't use float number.", "");
        } else if (Number(num) === 1) {
            num = prompt("Number should be greater than 1.", "");
        } else if (Number(num) <= 0) {
            num = prompt("Please enter a positive number that greater than 0.", "");
        }
    }

    return Number(num);
}

function getSimplyNumbers(numSimply) {
    let arrSimplyNum = [];

    nextNum:
        for (let i = 2; i <= numSimply; i++) {
            for (let j = 2; j < i; j++) {
                if (i % j == 0) {
                    continue nextNum;
                }
            }
            arr.push(i);
        }
    return arrSimplyNum;
}

let numIns = checkNumber(prompt('Please enter a second number', ''));
console.log(getSimplyNumbers(numIns));
