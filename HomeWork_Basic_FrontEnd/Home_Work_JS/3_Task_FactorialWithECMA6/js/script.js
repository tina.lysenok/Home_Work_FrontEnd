function checkNumber(num) {
    while (num == null
    || num === ""
    || isNaN(num)
    || !Number.isInteger(Number(num))
    || Number(num) === 1
    || Number(num) <= 0) {
        if (num == null || num === "" || isNaN(num)) {
            num = prompt("Enter a number value.", "");
        } else if (!Number.isInteger(Number(num))) {
            num = prompt("Please don't use float number.", "");
        } else if (Number(num) === 1) {
            num = prompt("Number should be greater than 1.", "");
        } else if (Number(num) <= 0) {
            num = prompt("Please enter a positive number that greater than 0.", "");
        }
    }

    return Number(num);
}

/*function getFactorial(num) {
    let numF = 1;
    for (let i = 1; i <= num; i++) {
        numF *= i;
    }
    return "Factorial of a number is " + numF;
}

let numIns = checkNumber(prompt('Please enter a number for factorial calculation:'));
console.log(getFactorial(numIns));*/

/*//ES5
let getFactorial5 = function(num) {
    let numF = 1;
    for (let i = 1; i <= num; i++) {
        numF *= i;
    }
    return "Factorial of a number is " + numF;
}

let numIns5 = checkNumber(prompt('Please enter a number for factorial calculation:'));
console.log(getFactorial5(numIns5));*/

//ES6
let getFactorial6 = (num) => {
    let numF = 1;
    for (let i = 1; i <= num; i++) {
        numF *= i;
    }
    // noinspection JSAnnotator
    return "Factorial of a number is " + numF;
}

let numIns6 = checkNumber(prompt('Please enter a number for factorial calculation:'));
console.log(getFactorial6(numIns6));
