let testObj = {
    'first': 'second',
    'second': 26,
    'third': true,
    'fourth': 13,
    'fifth':  {
        'a': {
            'f': 'f',
            'z': [11,12,13,14,15,16]
        },
        'b': 'b',
        'c': 'c',
        'd': [1,2,3,4,5,6,7,8,9,10]
    },
};

/*//Before
function recursionM(x) {
    let y = {};
    let key;
    for (key in x) {
        y[key] = x[key];
    }
    return y;
}

let newObjR = recursionM(testObj);
console.log(newObjR);*/

//After
function recursion(obj) {
    let key;
    let rtn = {};
    if(Array.isArray(obj)) {
        for (key in obj) {
            rtn[key] = recursion(obj[key]); //проверка на наличие Obj in Obj in Obj....проходимся по всем ключам
        }
    } else {
        return obj; // если obj не является typeOf(obj == 'Object') //начинаем все сначала_возвражаемся в рекурсию // мы ничего не копируем, если testObj = {k: 't'};
    }
    return rtn;
}

let newObj = recursion(testObj);
console.log(testObj);
console.log(newObj);

/*//ES5
let recursion5 = function recursion(obj) {
    let key;
    let rtn = {};
    if(Array.isArray(obj)) {
        for (key in obj) {
            rtn[key] = recursion(obj[key]); //проверка на наличие Obj in Obj in Obj....проходимся по всем ключам
        }
    } else {
        return obj; // если obj не является typeOf(obj == 'Object')
    }
    return rtn;
}

let newObj5 = recursion5(testObj);
console.log(testObj);
console.log(newObj5);*/

//ES6
let recursion6 = (obj) => {
    let key;
    let rtn = {};
    if(Array.isArray(obj)) {
        for (key in obj) {
            rtn[key] = recursion(obj[key]); //проверка на наличие Obj in Obj in Obj....проходимся по всем ключам
        }
    } else {
        // noinspection JSAnnotator
        return obj; // если obj не является typeOf(obj == 'Object') //если вхождение - это строка
    }
    // noinspection JSAnnotator
    return rtn;
}

let newObj6 = recursion6(testObj);
console.log(testObj);
console.log(newObj6);


