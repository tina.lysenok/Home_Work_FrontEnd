/*
Написать функцию createNewUser(), которая будет создавать и возвращать объект "пользователь".
При вызове функция должна спросить у вызывающего имя и фамилию.
Используя данные, введенные пользователем, создать объект со свойствами firstName и lastName.
Добавить в объект метод getLogin(), который будет возвращать первую букву имени пользователя,
соединенную с фамилией пользователя, все в нижнем регистре.
*/

function createNewUser(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.getLogin = function getLogin() {
        return this.firstName.slice(0, 1).toLowerCase() + this.lastName.toLowerCase();
    };
    return this;
}

let firstName = prompt('What is your first Name?', '');
let lastName = prompt('What is your last name?');
let firstUser = new createNewUser(firstName, lastName);
let userLogin = firstUser.getLogin();

console.log(firstUser);
console.log(userLogin);

/*
Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую.
Создать функции-сеттеры setFirstName()
и setLastName(), которые позволят изменить данные свйоства.
*/

function createNewUser2() {
    this.setFirstName = function setFirstName(value) {
        return this['firstName'] = value;
    };
    this.setLastName = function setLastName(value) {
        return this['lastName'] = value;
    };
    this.getLogin2 = function getLogin2() {
        return setFirstName.slice(0, 1).toLowerCase() + setLastName.toLowerCase();
    };
    return this;
}
let secondUser = new createNewUser2();
let setFirstName = secondUser.setFirstName('Maryna');
let setLastName = secondUser.setLastName('Litvinova');
let userLogin2 = secondUser.getLogin2();

console.log(secondUser); //get object
console.log(secondUser.firstName + ' ' + secondUser.lastName); //get string
console.log(userLogin2); //get string

//second method
const user = {
    firstName: 'Olga',
    lastName: 'Ivanova'
};
Object.defineProperty(user, 'login', {
   get: () => {
       return `${this.firstName.toLowerCase().charAt(0)}${this.lastName.toLowerCase()}`;
   }
});
console.log(user.login);