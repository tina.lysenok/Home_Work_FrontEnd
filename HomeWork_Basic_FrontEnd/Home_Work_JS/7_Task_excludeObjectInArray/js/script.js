/*function excludeBy(arr1, arr2, keyValue) {
    let actUsers = arr1.slice(0); //скопировали arr1 в actUsers
    actUsers.forEach(function(human, index) { //перебрали каждый элемент данного масива
        for (let i = 0; i < arr2.length; i++) {
            if(human[keyValue] == arr2[i][keyValue]) {
                actUsers.splice(index, actUsers.length);
            }
        }
    });
    console.log(actUsers);
}*/

function excludeBy(arr1, arr2, keyValue) {
    let actUsers = [];
    arr1.forEach(function (human, index) { //перебрали каждый элемент данного масива
        let isCoincidence = false;
        for (let i = 0; i < arr2.length; i++) {
            if (human[keyValue] == arr2[i][keyValue]) {
                isCoincidence = true; //удаляем данный эл из массива
            }
        }
        if (!isCoincidence) {
            actUsers.push(human); //если human[keyValue] !== arr2[i][keyValue],
            // то данный объект будет оставлен в исходном массиве
        }
    });
    console.log(actUsers);
}

let activeUsers = [
    {
        name: "Ivan",
        surname: "Ivanov",
        gender: "male",
        age: 30
    },
    {
        name: "Anna",
        surname: "Ivanova",
        gender: "female",
        age: 23
    },
    {
        name: "Anna",
        surname: "Shtil",
        gender: "female",
        age: 22
    },
    {
        name: "Lisa",
        surname: "Petrovna",
        gender: "female",
        age: 25
    }
  ];

let inactiveUsers = [
    {
        name: "Ivan",
        surname: "Ivanov",
        gender: "male",
        age: 30
    },
    {
        name: "Anna",
        surname: "Ivanova",
        gender: "female",
        age: 22
    }];


excludeBy(activeUsers, inactiveUsers, 'name');



















