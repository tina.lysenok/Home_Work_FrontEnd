//проверка на корректность даты
function isValidDate(date) {
    let matches = /^(\d{1,2})[\.](\d{1,2})[\.](\d{4})$/.exec(date);
    if (matches == null) {
        return false;
    }
    let d = matches[1];
    let m = matches[2] - 1;
    let y = matches[3];
    let composedDate = new Date(y, m, d);
    return composedDate.getDate() == d &&
        composedDate.getMonth() == m &&
        composedDate.getFullYear() == y;
}

function formatDate(date) {
    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();
    if (day < 10) {
        day = '0' + day;
    }
    if (month < 10) {
        month = '0' + month;
    }
    return day + '.' + month + '.' + year;
}

let dateNow = new Date();
let dateBirth = prompt('Please enter your Birth date', formatDate(dateNow));

//учитываем и проверяем валидность введенной даты пользователем
if (!isValidDate(dateBirth)) {
    alert('Date is invalid, plz enter valid date in dd.mm.yyyy format');
}

//форматирование даты рождения
let dateArr = dateBirth.split('.');
let dateOfBith = Number(dateArr[0]);
let monthOfBirth = Number(dateArr[1] - 1);
let yearOfBirth = Number(dateArr[2]);
let fullDateOfBirth = new Date(yearOfBirth, monthOfBirth, dateOfBith);
console.log(fullDateOfBirth);
//вывод к-ва лет пользователя
let fullYears = dateNow.getFullYear() - fullDateOfBirth.getFullYear();
alert('Your age is ' + fullYears);

//вывод обычного знака гороскопа
let symbol = ['', 'Aquarius', 'Pisces', 'Aries', 'Taurus', 'Gemini', 'Cancer', 'Leo', 'Virgo',
    'Libran', 'Scorpio', 'Sagittarius', 'Capricorn'];
let dates = [0, 20, 18, 20, 20, 20, 21, 22, 23, 22, 23, 22, 22];
let dd = dates[fullDateOfBirth.getMonth()];
let mm;
if (fullDateOfBirth.getDate() < dd) {
    if (fullDateOfBirth.getMonth() == 1) {
        mm = 12;
    } else {
        mm = fullDateOfBirth.getMonth();
    }
} else {
    mm = fullDateOfBirth.getMonth();
}
alert('Your sign is ' + symbol[mm]);

//вывод восточного знака гороскопа
let eastHoroscope = ['Обезьяна', 'Петух', 'Собака', 'Свинья', 'Крыса', 'Бык', 'Тигр', 'Заец',
    'Дракон', 'Змея', 'Лошадь', 'Овца'];
let modDateOfBirth = yearOfBirth % 12;
alert('Your eastern Horoscope is ' + eastHoroscope[modDateOfBirth]);





















